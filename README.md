# COVID-19 FAQ: Global Pandemic #



### What is COVID-19 FAQ: Global Pandemic? ###

**COVID-19 FAQ: Global Pandemic** is an online course that answers frequently asked questions 
about **COVID-19** with concise and scientifically verifiable explanations.

### How do I get access to the course? ###

* Go to the [Modulus website](https://modulusedu.com)
* Sign up and register as a student.
* Search for **COVID-19 FAQ: Global Pandemic** in the Biology subject category. 
* Enroll and take the course.
* It's that easy!


